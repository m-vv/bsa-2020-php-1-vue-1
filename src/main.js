import Vue from "vue";
import Buefy from "buefy";
import "buefy/dist/buefy.css";
import App from "./App.vue";

Vue.config.productionTip = false;
Vue.use(Buefy);
// eslint-disable-next-line no-unused-vars
function mounted() {
  if (localStorage.getItem("users")) {
    try {
      this.cats = JSON.parse(localStorage.getItem("users"));
    } catch (e) {
      localStorage.removeItem("users");
    }
  }
}

new Vue({
  render: h => h(App)
}).$mount("#app");
