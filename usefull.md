#First steps

##Binding of value
```vue
<h1>{{user.firstName}}</h1>  value from object directly in text
 <p v-bind:class="user.lastName">Check it one more time</p> value of attribute
  <em :class="user.firstName">Vue js can bind a lot of things</em>
```
shortcut v-bind:attribute   ~  :attribute

##Lifecycle Diagram

https://jsfiddle.net/nemoipaha/dswy5186/

hook we can call in js file directly https://vuejs.org/v2/guide/instance.html

```javascript
beforeCreate() {
}

created() {
console.log('we can access data from ${this.message})
}
```

## Calling JS code or methods of instance see more in directives

```vue
<button v-on:click="title = 'new title' ">Update title attribute from html</button>
<button v-on:click="$destroy()">Call intance method with $</button>
```


##Standard directives
v-text
v-html
v-show
v-if
v-else
v-else-if
v-for
v-on
v-bind
v-model
v-cloak
v-once

#f##or conditional output v-show  v-if v-else v-else-if
https://jsfiddle.net/nemoipaha/brotxhab/
```vue
<div v-if="type === 'A'">
    A
  </div>
  <div v-else-if="type === 'B'">
    B
  </div>
  <div v-else-if="type === 'C'">
    C
  </div>
  <div v-else>
    Not A/B/C
  </div>
in new Vue
data: {
 
    type: 'D',
    
  },
```
v-if completely remove element
v-show add display:none 
###Loops
https://jsfiddle.net/nemoipaha/w6juhLa0/
```vue
<template v-for="number in numbers">
    <span>{{ number }}</span>
    <span class="divider">|</span>
  </template>

	data: {
  	divider: 5,
	 	numbers: [10, 20, 30],

```
use v-for on the element that should repeated

! use :key in list construction for more optimal virtual DOM
###Loops v-on
https://jsfiddle.net/nemoipaha/nnq6rjLn/
```vue
 <button v-on:click="counter++">Clicked {{ counter }} times</button> 
 <button @dblclick="clickHandler">Dbl click button</button>
```
v-on: ~ @:  synonyms
for give parameters

```vue
@click="showMessage('Hello world', $event)">Show message</button>

in methods
    
    showMessage: function (message, event) {
      event.preventDefault()        	
      alert(message);
  	}
```
###Events
!!! To find information about event and chaine from child to parent
v-on:cloick
v-on:mousemove
etc

####event modifiers
.stop
.prevent
.capture
.self
.once

Order matters when using modifiers because the relevant code is generated in the same order. Therefore usning
@click.self.prevent will only prevent clicks ont her element itself, but
@click.prevent.self  will prevent all clicks.

####Event from keyboard

:keyup  :keydown
modifier only for specific button
keyup.enter  called only if enter data
we can use hot buttons
https://jsfiddle.net/nemoipaha/rfv8d8t4/

//TODO add hot keys for actions
```vue
  <div>
    <!-- Alt + C -->
    <label for="">Press alt+c to clear:</label>
    <input @keyup.alt.67="clear">         
  </div>
  
```

##computed props
https://jsfiddle.net/nemoipaha/q98Lzu8c/
They are functions which declared in new Vue and based on data via this pointer in app we can use them 
but when we change our data all computed method will be changed.
```vue
 computed: {
  	wordsCount: function() {
    	return this.text.split(' ').length;
    }
```
we can use computed props based on another computed props and  it recalculates if some part of them changed

Better to use computed props if we want save recourses because method called every time if something changes
calculated property recalcalated only if based values changes. 

!!! use for calculation of total members
!!! for sorting of users, for filtering

example of sorting with computed
https://codepen.io/akmur/pen/YQpgNL

Vue: Computed — вычисляемые свойства
https://medium.com/@modex13/vue-js-2-computed-%D0%B2%D1%8B%D1%87%D0%B8%D1%81%D0%BB%D1%8F%D0%B5%D0%BC%D1%8B%D0%B5-%D1%81%D0%B2%D0%BE%D0%B9%D1%81%D1%82%D0%B2%D0%B0-6649a2169b67


##Forms and v-model
https://jsfiddle.net/nemoipaha/cfj7yq1p/
v-model can create two way data binding form element and variables in app
```vue
<input type="checkbox" id="checkbox" v-model="checked" />
  <label for="checkbox">This label is {{ checked ? 'checked' : 'unchecked' }}</label>
```
v-bind for attaching value and show it
v-model for binding value and component
```vue
  <template v-for="dino in dinos">
    <label>
      <input type="radio" v-bind:value="dino" v-model="chosenDino">
      {{ dino }}
    </label>

<span>Favourite {{ chosenDino }}</span>
```
modificators
.lazy
```vue
<!-- synced after "change" instead of "input" -->
  <textarea v-model.lazy="message" placeholder="add multi lines"></textarea>
```
.trim
trim empty space

.number
evalate to number

##Styles
https://jsfiddle.net/nemoipaha/orudzob1/
active class name
isActive flag to add or not add style active
````vue
<div id="app">
  <div class="text" v-bind:class="{ active: isActive }">
````
we can add function that change styles.

##Vue nested reactive props
Vue cannot detect properly addition or deletion/ Since Vue performs the getter/setter conversion process
during instance initialization, a properly must be present in the data object in order for Vue to convert it and make
it reactive.
and new attritute prop for retroactive with this.$set, but better use null value even if it does not exist in begging
```vue
this.$set(this.user, 'age', 16);  
```
and for replace user 

```vue
replaceAnimal() {
    		this.$set(this.animals, 1, 'Fish');  
```

#books
https://habr.com/post/256965/
https://vuejs.org/v2/guide
http://blog.evanyou.me/2014/02/11/first-week-of-launching-an-oss-project/
examples from lector
https://jsfiddle.net/nemoipaha/q98Lzu8c/


testing in vueis
https://lmiller1990.github.io/vue-testing-handbook/#what-is-this-guide

#not vuejs
##Null
https://medium.com/javascript-in-plain-english/how-to-check-for-null-in-javascript-dffab64d8ed5
##styling
https://bulma.io/     https://buefy.org/
https://bulmatemplates.github.io/bulma-templates/


https://materialdesignicons.com/
https://bootstraptema.ru/index/competitors_bootstrap/0-12   Топ фреймворков для начинающих


Javascript - object key->value
https://stackoverflow.com/questions/5000953/javascript-object-key-value

hosting
https://paiza.cloud/en/
